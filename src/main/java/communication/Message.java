package communication;

public enum Message {

    ACTIVE_ID,
    CAPTOR_ID,
    OK,
    START,
    STOP;

    /**
     * Get value from text
     *
     * @param text
     * 		The text
     *
     * @return the value
     */
    public static Message getValue( String text )
    {
        // Look for
        for( Message out : Message.values( ) )
            if( Message.Name[ out.ordinal( ) ].equals( text ) )
                return out;

        // Can't find
        return null;
    }

    /**
     * Name of each incident type
     */
    public static final String[ ] Name =
            {
                    "ACT-",
                    "CAP-",
                    "OK",
                    "STA",
                    "STO"
            };
    /**
     * Maximal length
     */
    public static final int[ ][] id =
            {
                    {0,1,2},
                    {0,1,2,3,4},
                    {0},
                    {0},
                    {0}
            };
}
