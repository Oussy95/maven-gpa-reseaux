package communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener;


import java.util.Enumeration;

public class USB implements SerialPortEventListener{

    private String name;
    private String portName;
    private SerialPort serialPort;
    private String lastLine;
    private boolean isRead;
    
    /**
	* A BufferedReader which will be fed by a InputStreamReader 
	* converting the bytes into characters 
	* making the displayed results codepage independent
	*/
	private BufferedReader input;
	/** The output stream to the port */
	private static OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	
	private static final String PORT_NAMES[] = { 
			"COM3", // port1
                        "COM2", // port2 
			"COM3", // port3
			"COM10", // port4
	};
	
	public void initialize() {
        // the next line is for Raspberry Pi and 
        // gets us into the while loop and was suggested here was suggested https://www.raspberrypi.org/phpBB3/viewtopic.php?f=81&t=32186

        
		CommPortIdentifier portId = null;
		Enumeration portEnum;
		int index=0;
		do{
			//System.setProperty("gnu.io.rxtx.SerialPorts", PORT_NAMES[index]);
			portEnum = CommPortIdentifier.getPortIdentifiers();
			index++;
		}while( !portEnum.hasMoreElements());


		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
		// open serial port, and use class name for the appName.
		serialPort = (SerialPort) portId.open(this.getClass().getName(),
				TIME_OUT);
		// set port parameters
		serialPort.setSerialPortParams(DATA_RATE,
				SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1,
				SerialPort.PARITY_NONE);
		// open the streams
		input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
		output = serialPort.getOutputStream();
	
		// add event listeners
		serialPort.addEventListener(this);
		serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine=input.readLine();
				System.out.println(inputLine);
				if(inputLine.equals("0:1-2-3-4-5-6")){
					String mes = "STA";
					lastLine = "popo";
					isRead=true;
					write(mes.getBytes());
					//System.out.println("ici");
				}
				else if(inputLine.equals("STA-ON")){
					String mes = "STO";
					write(mes.getBytes());
					//System.out.println(oEvent.getEventType());
				}
				else if(inputLine.equals("ACT-1:ON")){
					String mes = "ST";
					write(mes.getBytes());
					//System.out.println("ici");
				}
				
			} catch (Exception e) {
				System.err.println("ici->"+oEvent.getSource());
				//oEvent.
				return;
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}

	public synchronized void serialEventDisconnect(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.FE) {
			try {
				
					System.out.println("Disconnect");
				
				
			} catch (Exception e) {
				System.err.println(oEvent.getEventType());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}
	
	public void write(byte[] s){
		try {
			output.write(s);
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    
	public USB(String name, String portName){

        this.name=name;
        this.portName=portName;
        this.isRead=false;
        
    }

    /*public static void main(String[] args) throws Exception {
		SerialTest main = new SerialTest();
		String message = "ID?";
		byte[] b = message.getBytes();
		main.initialize();
		Thread t=new Thread() {
			public void run() {
				//the following line will keep this app alive for 1000 seconds,
				//waiting for events to occur and responding to them (printing incoming messages to console).
				write(b);
				try {Thread.sleep(1000000);
				} catch (InterruptedException ie) {}
			}
		};
		t.start();
		System.out.println("Started");
	}*/
    
	
	
	
    public void send(){

    }

    public String getLastLine() {
		return lastLine;
	}

	public void setLastLine(String lastLine) {
		this.lastLine = lastLine;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public void read(){

    }
}
