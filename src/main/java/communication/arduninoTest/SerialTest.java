package communication.arduninoTest;

public class SerialTest {

	public static void main(String[] args) throws Exception {

		int cpt=0;
		ArduinoTest arduino0 = new ArduinoTest(0, 0);
		ArduinoTest arduino1 = new ArduinoTest(1, 1);
		arduino1.initialize();
		arduino0.initialize();
		

		arduino0.start();
		arduino1.start();
		System.out.println("threads Started");

		while (true) {
			if (cpt % 2 != 0) {
				arduino0.write("A");
				cpt++;
			} else {
				arduino1.write("B");
				cpt++;
			}

		}

	}
}