package communication;
import model.CorrespondTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;

import java.util.TreeMap;
import java.util.regex.*;

import gnu.io.*;
import model.Section;


public class Arduino implements SerialPortEventListener {
	private int id;
	private ArrayList<Integer> sections_id;
	private CorrespondTable correspondTable;
	private Message message;
	private final String START = "STA";
	private final String ID = "ID?";
	private String portName;
	SerialPort serialPort;



	public static final String PORT_ARDUINO_1="COM10";
	public static final String PORT_ARDUINO_2="COM8";


	private final String regex_ID = "([0-6]):([0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2})";// good
	private final Pattern pattern_ID = Pattern.compile(regex_ID);
	private boolean idIsDone;

	private final String regex_start = "^([STA]{3})-([0-6]):[ON]{2}";// good
	private final Pattern pattern_start = Pattern.compile(regex_start);

	private final String regex_stop = "^([STO]{3})-[0-6]:[ON]{2}";// good
	private final Pattern pattern_stop = Pattern.compile(regex_stop);

	private final String regex_active = "^([ACT]{3})-([0-9]{1,2}):[ON]{2}";// good
	private final Pattern pattern_active = Pattern.compile(regex_active);

	private final String regex_detect = "^([DCT]{3})-([0-9]{1,2}):[ON]{2}";// good
	private final Pattern pattern_detect = Pattern.compile(regex_detect);


	/**
	 * A BufferedReader which will be fed by a InputStreamReader
	 * converting the bytes into characters
	 * making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** The output stream to the port */
	private static OutputStream output= new OutputStream() {
		@Override
		public void write(int b) throws IOException {

		}
	};
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;

	public void initialize() {
		// the next line is for Raspberry Pi and
		// gets us into the while loop and was suggested here was suggested https://www.raspberrypi.org/phpBB3/viewtopic.php?f=81&t=32186
		idIsDone = false;
		sections_id= new ArrayList<Integer>();
		CommPortIdentifier portId = null;
		Enumeration portEnum;
		int index=0;
		do{
			//System.setProperty("gnu.io.rxtx.SerialPorts", PORT_NAMES[index]);
			portEnum = CommPortIdentifier.getPortIdentifiers();
			index++;
		}while( !portEnum.hasMoreElements());


		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			if (currPortId.getName().equals(portName)) {
				portId = currPortId;
				break;
			}

		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);
			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);
			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine=input.readLine();
				Matcher m = pattern_ID.matcher(inputLine);
				Matcher m1 = pattern_start.matcher(inputLine);
				Matcher m2 = pattern_stop.matcher(inputLine);
				Matcher m3 = pattern_active.matcher(inputLine);
				Matcher m4 = pattern_detect.matcher(inputLine);
				if(m.matches())
				{
					if(!this.idIsDone) {
						int id = Integer.parseInt(m.group(1));
						String[] section = m.group(2).split("-");
						for (String s : section) {
							this.sections_id.add(Integer.parseInt(s));
						}

						idIsDone = true;
						initTab(id,this.sections_id); // change the init
					}
				}
				else if(m1.matches())
				{
					System.out.println("STA-ON : ici");
					//writeTest("ACT-1");
				}
				else if(m2.matches())
				{
					System.out.println("STOP: ici");
				}
				else if(m3.matches())
				{
					int id = Integer.parseInt(m.group(2));
					String confirm = m.group(3);
					if(confirm=="ON")
					{
						// envoyer l'information de la detection du capteur
						System.out.println("active alimentation sur le secteur n°"+Integer.toString(id));
					}


				}
				else if(m4.matches())
				{
					int id = Integer.parseInt(m.group(2));
					String confirm = m.group(3);
					if(confirm=="ON")
					{
						// envoyer l'information de la detection du capteur
						System.out.println("train sur capteur n°"+Integer.toString(id));
					}
				}

			} catch (Exception e) {
				System.err.println("ici->"+oEvent.getSource());
				//oEvent.
				return;
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}

	public synchronized void serialEventDisconnect(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.FE) {
			try {

				System.out.println("Disconnect");


			} catch (Exception e) {
				System.err.println(oEvent.getEventType());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}

	public synchronized void initTab(int idFromArduino,ArrayList sections)
	{


		this.correspondTable = new CorrespondTable(idFromArduino, sections, sections);

	}

	public synchronized void updateTab()
	{
		this.correspondTable.update();
	}

	public synchronized void startArduino(){
		try {
			byte[] b = START.getBytes();
			output.write(b);
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void askForIdArduino(){
		try {
			byte[] b = ID.getBytes();
			output.write(b);
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void writeTest(String test){
		try {
			byte[] b = test.getBytes();
			output.write(b);
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Arduino(int id,String portName){
		this.id=id;
		this.portName= portName;

	}

	public int getId() {
		return id;
	}

	public static void main(String[] args) throws Exception {
		Arduino a = new Arduino(0,PORT_ARDUINO_1);
		Arduino b = new Arduino(1, PORT_ARDUINO_2);
		a.initialize();
		b.initialize();
		Thread t=new Thread() {
			public void run() {

				//the following line will keep this app alive for 1000 seconds,
				//waiting for events to occur and responding to them (printing incoming messages to console).
				//write(b);
				a.askForIdArduino();

				try {Thread.sleep(1000000);
				} catch (InterruptedException ie) {}
			}
		};
		t.start();
		System.out.println("Started");
	}



}
