package cam;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Coordonee")
@XmlAccessorType(XmlAccessType.FIELD)
public class Coordonee {
	private String id_train;
	private String X_coo;
	private String Y_coo;
	/**
	 * @param id_train
	 * @param x_coo
	 * @param y_coo
	 */
	public Coordonee(String id_train, String x_coo, String y_coo) {
		super();
		this.id_train = id_train;
		X_coo = x_coo;
		Y_coo = y_coo;
	}
	/**
	 * 
	 */
	public Coordonee() {
		super();
	}
	/**
	 * @return the id_train
	 */
	public String getId_train() {
		return id_train;
	}
	/**
	 * @param id_train the id_train to set
	 */
	public void setId_train(String id_train) {
		this.id_train = id_train;
	}
	/**
	 * @return the x_coo
	 */
	public String getX_coo() {
		return X_coo;
	}
	/**
	 * @param x_coo the x_coo to set
	 */
	public void setX_coo(String x_coo) {
		X_coo = x_coo;
	}
	/**
	 * @return the y_coo
	 */
	public String getY_coo() {
		return Y_coo;
	}
	/**
	 * @param y_coo the y_coo to set
	 */
	public void setY_coo(String y_coo) {
		Y_coo = y_coo;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Coordonee [id_train=" + id_train + ", X_coo=" + X_coo + ", Y_coo=" + Y_coo + "]";
	}
}
