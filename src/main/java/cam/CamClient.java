package cam;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class CamClient {

	public static void main(String[] args) {
		Client client = Client.create();

		WebResource webResource = client.resource("http://127.0.0.1:5000/");

		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

		// statut == 200 OK
		if (response.getStatus()!= 200) {
			System.out.println("HTTP Echoue"+response.getStatus());
			String error = response.getEntity(String.class);
			System.out.println("Error"+error);
			return;
		}
		String outPut = response.getEntity(String.class);
		
		try {
			//Transform to JSON Object
			JSONObject outPutJSON = new JSONObject(outPut);
			JSONArray cooArray = (JSONArray) outPutJSON.get("Coordonee");
			//get DATA
			ArrayList<Coordonee> listeCoor = new ArrayList<>();
			for (int i = 0; i < cooArray.length(); i++) {
				JSONObject data = (JSONObject) cooArray.get(i);
				//transform to java Object
				Coordonee c = new Coordonee();
				c.setId_train(data.get("id_train").toString());
				c.setX_coo(data.get("X_coo").toString());
				c.setY_coo(data.get("Y_coo").toString());
				listeCoor.add(c);
				System.out.println(c.toString());
			}
		}catch (Exception err){
			System.out.println(err.getMessage());
		}		
		
	}
}