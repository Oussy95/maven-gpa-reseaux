package dashboard;

/**
 * classe test qui permeterra de persister les données sensors (juste un exmple)
 * @author sofiane
 *
 */
public class DataSensors {

	/**
	 * DATA aquises
	 */
	private String location;
	private String temp;
	private String date;
	private String idSensor;
	/**
	 * @param location
	 * @param temp
	 * @param date
	 * @param idSensor
	 */
	public DataSensors(String location, String temp, String date, String idSensor) {
		super();
		this.location = location;
		this.temp = temp;
		this.date = date;
		this.idSensor = idSensor;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the temp
	 */
	public String getTemp() {
		return temp;
	}
	/**
	 * @param temp the temp to set
	 */
	public void setTemp(String temp) {
		this.temp = temp;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the idSensor
	 */
	public String getIdSensor() {
		return idSensor;
	}
	/**
	 * @param idSensor the idSensor to set
	 */
	public void setIdSensor(String idSensor) {
		this.idSensor = idSensor;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DataSensors [location=" + location + ", temp=" + temp + ", date=" + date + ", idSensor=" + idSensor
				+ "]";
	}
	
	
	
	
	
}
