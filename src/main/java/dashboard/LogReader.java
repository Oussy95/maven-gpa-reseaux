package dashboard;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class LogReader {


	/**
	 * Lire un fichier text(log)
	 * @param fileName
	 * @param path
	 */
	public static void readLogFile(String fileName) {
		String line = null;
		try {
			final String dir = System.getProperty("user.dir");
		    
			FileReader fileReader = new FileReader(dir+"\\logSource\\"+fileName);
		
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			while((line = bufferedReader.readLine())!=null) {
				System.out.println(line);
			}
		}catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public static void main(String[] args) {
		
		readLogFile("test.txt");
		 
	}

}
