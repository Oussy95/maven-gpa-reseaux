package test;

import model.Rail;
import model.Simulation;
import topology.PathFinder;
import topology.xml.*;

public class PathFinding {

    public static void main(String[] args) {

        Simulation simulation = new Simulation();

//        for (int index = 0;index < simulation.getMaxRailNb()-1; index++) {
//            rails[index] = simulation.getRail(index);
//            //System.out.println(rails[index]);
//        }
        int lastRail = simulation.getRails().lastKey();

        final int ax = simulation.getRail(1).getX();
        final int ay = simulation.getRail(1).getY();
        final int bx = simulation.getRail(15).getX();
        final int by = simulation.getRail(15).getY();

        System.out.println(ax+" "+ay+" "+bx+" "+by);

        PathFinder pathFinder = new PathFinder(new Map(), ax, ay, bx, by);
        pathFinder.setMapStart(simulation.getRail(1));
        pathFinder.setMapStop(simulation.getRail(89));

        //System.out.println(pathFinder.getMapStart()+" "+pathFinder.getMapStop());

        long start = System.currentTimeMillis();
        if(pathFinder.findPath()) {
            System.out.println("Path length: "+pathFinder.getPathList().size()+" "+pathFinder.getPathList());
			/*for (p el :path.getPathList()){
				System.out.println("x: "+el.x+"y: "+el.y);
			}*/
        }
        else System.out.println("Aucun chemin trouvé");
        System.out.println("+Excel time : " + String.valueOf(System.currentTimeMillis() - start));
    }
}
