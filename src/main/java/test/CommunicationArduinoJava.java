package test;


import communication.Arduino;

public class CommunicationArduinoJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final Arduino a = new Arduino(0,"COM3");
		a.initialize();
		Thread t=new Thread() {
			public void run() {

				//the following line will keep this app alive for 1000 seconds,
				//waiting for events to occur and responding to them (printing incoming messages to console).
				//write(b);
				a.askForIdArduino();
				try {Thread.sleep(1000000);
				} catch (InterruptedException ie) {}
			}
		};
		t.start();
		System.out.println("Started");
	}

}
