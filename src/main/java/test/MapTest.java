package test;

import topology.xml.*;

public class MapTest {

    public static void main(String args[]) {
        try {
            //Map map = new Map("mapRand.xml");
            MapToXml mapToXml = new MapToXml();
            XmlToMap xmlToMap = new XmlToMap();
            int nbNode = mapToXml.nbRandom(6, 7);
            int[][] matrice = mapToXml.createMatrice(nbNode);
            int[][] mat = new int[4][4];
            for(int i = 0; i < mat.length; i++) {
                for(int j = 0; j < mat.length; j++) {
                    if(i == j) mat[i][j] = 0;
                    else if((i == 0 && j == 2) || (i == 1 && j == 3) || (i == 2 && j == 0) || (i == 3 && j == 1)) {
                        mat[i][j] = 0;
                    }
                    else mat[i][j] = 5;
                }
            }
            for (int[] ints : mat) {
                for (int j = 0; j < mat.length; j++) {
                    System.out.print(ints[j] + "\t");
                }
                System.out.println();
            }
            mapToXml.createRandomMap("mapCarre.xml", mat);

            /*Map map2 = xmlToMap.loadMap("mapCarre.xml");
            int cptRail = 0;
            for(int i = 0; i < map2.getNbSections(); i++) {
                for(int j = 0; j < map2.getSections().get(i).getRails().size(); j++) {
                    System.out.println(map2.getSections().get(i).getRail(cptRail)+" a pour voisin destination "+map2.getSections().get(i).getRail(cptRail).getRailsDestination());
                    cptRail++;
                }
            }*/
        } catch (Exception e) {
            System.err.println("build map error");
            e.printStackTrace();
        }

    }
}
