package model;

import communication.Arduino;
import topology.PathFinder;
import topology.xml.Map;
import topology.xml.XmlToMap;

import java.util.ArrayList;
import java.util.TreeMap;

public class Simulation {

    private int idTrainsNb;
    private int idRailsNb;
    private int idCaptorsNb;
    private int idArduinosNb;

    private int maxTrainnb;
    private int maxRailNb;
    private int maxCaptorNb;
    private int maxArduinoNb;

    private int SIZE_RAIL=2;

    private int currentRail=1,currentRail2=2,currentRail3=3;



    private TreeMap<Integer,Train> trains;
    private TreeMap<Integer,Section> sections;
    private TreeMap<Integer, Rail> rails;
    private TreeMap<Integer, Captor> captors;
    ArrayList<Rail> railsNotInsert=new ArrayList<Rail>();


    private int[] coordonateX={ 0,-10,-20,-30, //section 1 0-3//////////////
            -40,-50,-60,-70,//section 2 4-7/////////////
            -80,-90,-100,-110,-120,-130,-140,-150,-160,-170,-180,-190,-200,-210, //section 6 8-21///////////
            -220,-230,-240,-250,-260,-270,-280, //section 8 22-28//////////
            -290,-290,-290,-290,-290,-290,-290,-290, //section 12 29-36/////////////////
            -290,-290,-290,-290,-280,-270,-260, // section 17 37-43//////////
            -250,-240,-230,-220,-210,-200,-190,-180,-170,-160,//section 21 44-53///////////
            -150,-140,-130,-120,-110,-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,10,//section 22 54-69/////////////
            20,30,30,30,30,30,30,30,30,30,30, //section 11 70-80/////////////
            20,10,0,-10,-20,-30,-40,-50,-60, //section 3 81-89/////////
            -200,-210,-220,-220,-210,-200,-190,-180,-170, //section 10 90-98/////////
            -230,-220,-210,-200,-190,-180,-160,-150,-140,-130,-120, //section 14 99-109
            -260,-260,-250,-240,-230,-220,-210, //section 18 110-116//////////
            -200,-190,-180,-170,-160,-150,-140,-130, //section 26 117-124//////////
            20,10,0,-10,-20,-30,-40,-50,-60, //section 2 125-133/////////////
            -70,-70,-80,-90,-100,-110,-120,-130,-140,-150,-160,-170,-180,-190, //section 7 134-147//////////////
            -200,-210,-220,-230,-240,-250,-260, //section 9 148-154////////////
            -270,-270,-270,-270,-270,-270,-270,-270, //section 13 155-162////////////
            -270,-270,-260,-260,-250,-240, //section 16 163-168///////////////
            -230,-220,-210,-200,-190,-180,-170,-160,-150,-140, //section 19 169-178///////////////
            -130,-120,-110,-100,-90,-80,-70,-60,-50,-40,-30,-20, //section 23 179-190/////////////
            -10,0,10,10,10,10,10,10,10,10,0, //section 25 191-201///////////////////
            -10,-20,-30,-40,-50,-60, //section 5 202-207////////////////
            -220,-240,-240,-240,-240,-240,-230,-220, //section 11 208-215/////////
            -220,-210,-200,-190,-180,-170,-160,-150,-140,-130,-120,-110, //section 15 216-227
            -200,-190,-180,-170,-160,-150,-140,-130, //section 20 228-235
            -70 //section 4 236 /////////
    };
    private int[] coordonateY={ 0,0,0,0,//section 1 0-3///////////
            0,0,-10,-10,//section 2 4-7///////////////
            -10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10, //section 6 8-21//////////////
            -10,-10,-10,-10,-10,-10,-20, //section 8 22-28///////////////
            -30,-40,-50,-60,-70,-80,-90,-100, //section 12 29-36//////////////////
            -110,-120,-130,-140,-150,-150,-150, // section 17 37-43///////////
            -160,-160,-160,-160,-160,-160,-160,-160,-160,-160, //section 21 44-53////////////
            -160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-160,-150, //section 22 54-69//////////
            -140,-130,-120,-110,-100,-90,-80,-70,-60,-50,-40, //section 11 70-80//////////////////
            -30,-30,-30,-30,-30,-30,-30,-30,-30, //section 3 81-89////////////
            -40,-50,-60,-70,-70,-70,-70,-70,-70, //section 10 90-98////////////
            -80,-80,-90,-90,-100,-100,-110,-110,-120,-130,-130, //section 14 99-109
            -160,-170,-180,-190,-190,-190,-190, //section 18 110-116////////////////
            -190,-190,-190,-190,-190,-190,-190,-190, //section 26 117-124///////////////
            -20,-20,-20,-20,-20,-20,-20,-20,-20, //section 2 125-133/////////////
            -20,-30,-30,-30,-30,-30,-30,-30,-30,-30,-30,-30,-30,-30, //section 7 134-147///////////
            -30,-30,-30,-30,-30,-30,-30, //section 9 148-154///////////
            -30,-40,-50,-60,-70,-80,-90,-100, //section 13 155-162/////////////
            -110,-120,-120,-130,-130,-130, //section 16 163-168//////////////
            -140,-140,-140,-140,-140,-140,-140,-140,-140,-140, //section 19 169-178//////////////
            -140,-140,-140,-140,-140,-140,-140,-140,-140,-140,-140,-140, //section 23 179-190/////////////
            -140,-130,-120,-110,-100,-90,-80,-70,-60,-50,-40, //section 25 191-201//////////////////
            -40,-40,-40,-40,-40,-40, //section 5 202-207/////////////////////
            -20,-40,-50,-60,-70,-80,-90,-90, //section 11 208-215///////////
            -90,-90,-90,-90,-100,-100,-100,-100,-110,-110,-110,-120, //section 15 216-227
            -150,-150,-150,-160,-160,-160,-160,-160, //section 20 228-235
            -20, //section 4 236*////////
    };

    private Map map;
    private PathFinder finder;


    public Simulation(){
        idTrainsNb=0;
        idRailsNb=0;
        idCaptorsNb=0;
        idArduinosNb=0;
        trains=new TreeMap<Integer, Train>();
        sections=new TreeMap<Integer, Section>();
        rails=new TreeMap<Integer, Rail>();
        captors = new TreeMap<>();
        init();
    }




    public void init(){

        XmlToMap xmlToMap = new XmlToMap();
        map = xmlToMap.loadMap("map.xml");
        //utilisation des sections

        finder=new PathFinder(map);
        //utilisation des trains

        //maxTrainnb=pour indiquer le nombre de train;
        maxTrainnb=map.getNbTrains();


        for(int index=0;index < map.getNbTrains();index++) {
            Train train = new Train(index);
            trains.put(train.getId(),train);
        }

        int cptRail=0;
        maxRailNb=0;
        for(int index=0;index < map.getSections().size();index++){
                Section section=map.getSections().get(index);
                addCaptor(map.getSections().get(index).getCaptor());
                maxRailNb+=section.getRails().size();
                for(int index2 = 0; index2 < section.getRails().size(); index2++){
                    int idsec=verifyRail(cptRail);
                    Rail rail= map.getSections().get(idsec).getRail(cptRail);
                    cptRail=addRail(rail,cptRail);
                    // Attention vérifier si c'est sub ou cross ou normal
                    // System.out.println("valeur du subRail"+rail.getClass().equals(SubRail.class));


                }
        }




        Train train;
        for(int index=0;index<trains.size();index++) {
            train=trains.get(index);
           train.setX(rails.get(index).getX());
            trains.get(index).setY(rails.get(index).getY());

        }



    }

    public void addCaptor(Captor captor) {
        captors.put(captor.getId(), captor);
    }

    public int addRail(Rail rail,int cptRail){

        rail.setX(coordonateX[cptRail]*SIZE_RAIL);
        rail.setY(coordonateY[cptRail]*SIZE_RAIL);
        rails.put(rail.getId(),rail);

        //System.out.println((cptRail));
        cptRail++;
        return cptRail;
    }


    private int verifyRail(int id){
        int size=map.getSections().size();
        for(int index=0;index<size;index++){
            if(map.getSections().get(index).getRail(id)!=null){
                return index;
            }
        }
        return -1;
    }



    public ArrayList<Rail> itinerary(Train train, int start, int end, int way){
        //int lastRail = this.getRails().lastKey();

        final int ax = this.getRail(start).getX();
        final int ay = this.getRail(start).getY();
        final int bx = this.getRail(end).getX();
        final int by = this.getRail(end).getY();

        trains.get(train.getId()).setX(ax);
        trains.get(train.getId()).setX(ay);

        System.out.println(ax+" "+ay+" "+bx+" "+by);

        PathFinder pathFinder = new PathFinder(map, ax, ay, bx, by);
        pathFinder.setMapStart(this.getRail(start));
        pathFinder.setMapStop(this.getRail(end));

        //System.out.println(pathFinder.getMapStart()+" "+pathFinder.getMapStop());

        long time = System.currentTimeMillis();
        if(pathFinder.findPath(way)) {
            System.out.println("Path length: "+pathFinder.getPathList().size()+" "+pathFinder.getPathList());
            System.out.println("+Time find path : " + String.valueOf(System.currentTimeMillis() - time));
            return pathFinder.getPathList();
			/*for (p el :path.getPathList()){
				System.out.println("x: "+el.x+"y: "+el.y);
			}*/
        }
        else System.out.println("Aucun chemin trouvé");
        System.out.println("+Time find path : " + String.valueOf(System.currentTimeMillis() - time));
        return null;
    }





    /**
     *
     * @param id
     * @param targetRail la rail du train
     *
     *                Actuellement le train ne se déplace que linéairement par saut de 1
     */
    public int moveTrain(int id,int targetRail){
        Train train=trains.get(id);

        int testX=rails.get(targetRail).getX()-train.getX();
        int testY=rails.get(targetRail).getY()-train.getY();

        /* Test afin de savoir quel direction prendre pour avancer*/
        if(testX >0){
            train.setX(train.getX()+1);
        }
        else if(testX <0){
            train.setX(train.getX()-1);
        }

        if(testY >0){
            train.setY(train.getY()+1);
        }
        else if(testY <0){
            train.setY(train.getY()-1);
        }

        if(testX ==0 && testY==0){
            //par la suite il peut correspondre a l'id du rail et nom a sa position
            //targetRail.hasTrain();
            System.out.println("Train "+train.getId()+" arrived at rail "+targetRail+":"+rails.get(targetRail).getX()+";"+rails.get(targetRail).getY());
            return 1;
        }
        return 0;
    }

    public Captor getCaptor(int id) {
        Captor captor = captors.get(id);
        if(captor!= null)
            return captor;
        System.out.println("arduino inexistant");
        return null;
    }

    public Rail getRail(int id){
        Rail rail= rails.get(id);
        if(rail!=null){
            return rail;
        }
        System.err.println("rail inexistante");

        return null;
    }

    public Train getTrain(int id){
        Train train=trains.get(id);
        if(train!=null){
            return train;
        }
        System.err.println("train inexistante");

        return null;
    }

    /**
     * Permet de faire avancer les train selon ses itiniréaires
     */
    public void run(){
        //graphe =>
        ///attention au sens
        ArrayList<ArrayList<Rail>> itineraries = new ArrayList<>();
        ArrayList<Integer> captors = new ArrayList<>();

        Arduino arduino = new Arduino(0, "COM10");

        Captor captor = new Captor(0);
        captors.add(0);


        ArrayList<Integer> sections = new ArrayList<>();
        sections.add(0);
        sections.add(1);
        sections.add(2);
        sections.add(3);
        sections.add(4);


        CorrespondTable correspondTable = new CorrespondTable(0, sections, captors);

        ArrayList<Rail> itinerary0 = itinerary(trains.get(0), 1, 15, 1);
        ArrayList<Rail> itinerary1 = itinerary(trains.get(1),141, 222, 1);
        ArrayList<Rail> itinerary2 = itinerary(trains.get(0),15, 1, 0);
        ArrayList<Rail> itinerary3 = itinerary(trains.get(1),222, 141, 0);
        ArrayList<Rail> itinerary4 = itinerary(trains.get(0),1, 112, 1);

        itineraries.add(itinerary0);
        itineraries.add(itinerary1);
        itineraries.add(itinerary2);
        itineraries.add(itinerary3);
        itineraries.add(itinerary4);

        itineraries.get(0).get(3).getSection().setCaptor(captor);

        /*trains.get(0).setX(itineraries.get(0).get(0).getX());
        trains.get(0).setX(itineraries.get(0).get(0).getY());
        trains.get(1).setX(itineraries.get(1).get(0).getX());
        trains.get(1).setX(itineraries.get(1).get(0).getY());*/

        for(int i = 0; i < itineraries.size(); i++) {
            for(int j = 0; j < itineraries.get(i).size(); j++) {
                for (int index = 0; index < trains.size(); index++) {
                    if((i == 0 || i == 2 || i == 4) && index == 0) {
                        if(i == 0 && j == 3) {
                            //itineraries.get(i).get(j).getSection().getCaptor().activate();
                            map.getSections().get(itineraries.get(i).get(j).getSection().getId()).getCaptor().activate();
                        }
                        moveTrain(index, itineraries.get(i).get(j).getId());
                    }
                    else if ((i == 1 || i == 3) && index == 1)
                        moveTrain(index, itineraries.get(i).get(j).getId());
                    //moveTrain(index, itineraries.get(i).get(j).getId());
                    /*int val = moveTrain(index, (rails.get(currentRail2).getRailsDestination().get(0).getId()));
                    if (val == 1) {
                        currentRail2 = rails.get(currentRail2).getRailsDestination().get(0).getId();
                    }*/
                }
                boolean[] verifCapt = verifyCaptor();
                System.out.println(verifCapt.length);
                for (boolean v : verifCapt) {
                    if (v) {
                        arduino.writeTest("CAP");
                        System.out.println("CAP");
                    }
                }
                map.getSections().get(itineraries.get(i).get(j).getSection().getId()).getCaptor().disctivate();
            }
        }

        // cpt=moveTrain(0,(rails.get(currentRail).getRailsDestination().get(0).getId()));
       // currentRail=rails.get(currentRail).getRailsDestination().get(0).getId();


    }


    /**
     *
     * @return les capteurs activer
     */
    public boolean[] verifyCaptor(){
        boolean[] captors=new boolean[map.getSections().size()];
        for(int index=0;index<map.getSections().size();index++){
            captors[index]=map.getSections().get(index).getCaptor().isActive();
        }
        return captors;
    }


    public TreeMap<Integer, Captor> getCaptors() {
        return captors;
    }

    public void setArduinos(TreeMap<Integer, Captor> arduinos) {
        this.captors = captors;
    }

    public TreeMap<Integer, Section> getSections() {
        return sections;
    }

    public void setSections(TreeMap<Integer, Section> sections) {
        this.sections = sections;
    }

    public TreeMap<Integer, Rail> getRails() {
        return rails;
    }

    public void setRails(TreeMap<Integer, Rail> rails) {
        this.rails = rails;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public int getMaxTrainnb() {
        return maxTrainnb;
    }



    public int getMaxRailNb() {
        return maxRailNb;
    }


}
