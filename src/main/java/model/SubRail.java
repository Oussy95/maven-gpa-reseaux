package model;

public class SubRail extends Rail{
    private int switcher;

    public SubRail(int id, int x, int y, double sourceAngle, double curvature) {
        super(id, x, y, sourceAngle, curvature);
        this.switcher = 0;
        this.setLength(0);
    }
    public SubRail(int id){
        super(id);
        this.switcher=0;
        this.setLength(0);
    }

    /**
     *
     * @return la valeur de l'aiguillage
     */
    public int getSwitcher() {
        return switcher;
    }

    /**
     *
     * @param switcher la valeur de l'aiguillage
     *
     *                 Attention elle doit pas dépasser la taille du nombre d'aiguille
     */
    public void setSwitcher(int switcher) {
        this.switcher = switcher;
    }



}
