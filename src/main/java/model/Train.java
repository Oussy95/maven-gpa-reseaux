package model;

/**
 * Le train est représenté par un point présent sur le reseau
 */


public class Train {

	private int id;
	private int x;//coordonnee x
	private int y;//coordonnee y
	private int curvature;//courbure

	public Train(int id){
		this.id=id;
	}

	public Train(int id,int x,int y){
		this.id=id;
		this.x=x;
		this.y=y;
		this.curvature=curvature;

	}

	public Train(int id,int x,int y,int curvature){
		this.id=id;
		this.x=x;
		this.y=y;
		this.curvature=curvature;

	}



	public int getId() {
		return id;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getCurvature() {
		return curvature;
	}

	public void setCurvature(int curvature) {
		this.curvature = curvature;
	}

	@Override
	public String toString() {
		return "Train{" +
				"id=" + id +
				", x=" + x +
				", y=" + y +
				", curvature=" + curvature +
				'}';
	}
}
