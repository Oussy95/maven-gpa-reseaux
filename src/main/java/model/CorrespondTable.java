package model;

import java.util.ArrayList;

public class CorrespondTable {

    private int idArduino;
    private ArrayList<Integer> sections;
    private ArrayList<Integer> captors;




    public CorrespondTable(int idArduino,ArrayList sections,ArrayList captors){
        this.idArduino= idArduino;
        this.sections= sections;
        this.captors=captors;
    }


    public void update() 
    {
    	System.out.println("update");
    }


    public int getArduino() {
        return idArduino;
    }



    public ArrayList<Integer> getSections() {
        return sections;
    }


    public ArrayList<Integer> getCaptors() {
        return captors;
    }


    public boolean isId(int id){
        if(sections.get(id)!=null){
            return true;
        }
        return false;
    }

}
