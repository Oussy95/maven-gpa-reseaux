package model;

public class Captor {
    private int id;
    private boolean isOn;
    private boolean isActive;


    public Captor(int id){
        this.id=id;
        isOn=true;
        isActive=false;
    }

    public int getId() {
        return id;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn() {
        if(!isOn){
            isOn=true;
        }
    }

    public void setOff() {
        if(isOn){
            isOn=false;
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public void activate() {
        if(!isActive){
            isActive = true;
        }
    }

    public void disctivate() {
        if(isActive){
            isActive = false;
        }
    }
}
