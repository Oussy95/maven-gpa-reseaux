package model;

import java.util.ArrayList;

public class CrossRail extends Rail {

    public CrossRail(int id, int x, int y, double sourceAngle, double curvature) {
        super(id, x, y, sourceAngle, curvature);

        setLength(0);
    }
    public CrossRail(int id){
        super(id);

        setLength(0);
    }

}
