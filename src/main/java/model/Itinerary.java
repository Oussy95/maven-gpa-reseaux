package model;

import java.util.ArrayList;

public class Itinerary {


    ArrayList<Rail> rails = new ArrayList<>();
    Train train;


    public Itinerary(ArrayList<Rail>rails,Train train){
        this.rails=rails;
        this.train=train;
    }


    public ArrayList<Rail> getRails() {
        return rails;
    }

    public void setRails(ArrayList<Rail> rails) {
        this.rails = rails;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }
}
