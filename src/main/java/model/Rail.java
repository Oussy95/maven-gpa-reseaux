package model;

import java.util.ArrayList;

public class Rail{

	private int id;
	private String name;
	private ArrayList<Rail> railSource = new ArrayList<Rail>();
	private ArrayList<Rail> railDestination = new ArrayList<Rail>();
	private Section section = new Section();
	private int length;
	private double sourceAngle;
	private int x;
	private int y;
	private double curvature;
	private boolean hasTrain;



	public Rail(int id){
		this.id=id;
	}

	public Rail(int id,String name){
		this.id=id;
	}

	/**
	 *
	 * @param id id de la rail
	 * @param name nom dans le cas d'une station
	 * @param section section de la rail
	 * @param x coordonnée source x
	 * @param y coordonnée source y
	 * @param length longueur
	 * @param sourceAngle angle source
	 * @param curvature angle de courbure
	 */
	public Rail(int id,String name,Section section,int x, int y,int length,double sourceAngle,double curvature) {
		this.id = id;
		this.name=name;
		this.section = section;
		this.railSource = new ArrayList<Rail>();
		this.railDestination= new ArrayList<Rail>();
		this.x=x;
		this.y=y;
		this.length=length;
		this.sourceAngle=sourceAngle;
		this.curvature=curvature;

	}



	public Rail(int id,int x, int y,int length,double sourceAngle,double curvature){
		this.id = id;
		this.name=null;
		this.railSource = new ArrayList<Rail>();
		this.railDestination= new ArrayList<Rail>();
		this.x=x;
		this.y=y;
		this.length=length;
		this.sourceAngle=sourceAngle;
		this.curvature=curvature;

	}

	public Rail(int id, int x, int y, double sourceAngle, double curvature) {
		this.id = id;
		this.railSource = new ArrayList<Rail>();
		this.railDestination= new ArrayList<Rail>();
		this.x=x;
		this.y=y;
		this.sourceAngle=sourceAngle;
		this.curvature=curvature;

	}

	public int getId() {
		return id;
	}


	public ArrayList<Rail> getRailsSource() {
		return railSource;
	}



	public ArrayList<Rail> getRailsDestination() {
		return railDestination;
	}

	public void addRailSource(Rail railSource) {
		this.railSource.add(railSource);
	}

	public void addRailDestination(Rail railDestination) {
		this.railDestination.add(railDestination);
	}


	public void setRailSource(ArrayList<Rail> railSource) {
		this.railSource = railSource;
	}

	public void setRailDestination(ArrayList<Rail> railDestination) {
		this.railDestination = railDestination;
	}


	public Section getSection() {
		return section;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public double getSourceAngle() {
		return sourceAngle;
	}

	public void setSourceAngle(double sourceAngle) {
		this.sourceAngle = sourceAngle;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double getCurvature() {
		return curvature;
	}

	public void setCurvature(double curvature) {
		this.curvature = curvature;
	}

	public boolean isStation(){
		return name !=null;
	}

	public boolean hasTrain(){
		return hasTrain;
	}

	public void putTrain(){
		hasTrain=true;
	}
	public void removeTrain(){
		hasTrain=false;
	}



	@Override
	public String toString() {
		return "Rail{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
