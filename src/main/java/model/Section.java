package model;

import java.util.*;

public class Section {
    private int id;
    private boolean hasStation;
    private TreeMap<Integer,Rail> rails;
    private Captor captor = new Captor(id);
    private int x;
    private int y;
    private double sourceAngle;
    private double curvature;
    private Rail railCaptor;

    public Section(int id, boolean hasStation, Captor captor, int x, int y, double curvature) {
        this.id = id;
        this.hasStation = hasStation;
        this.rails = new TreeMap<Integer, Rail>();
        this.captor = captor;
        this.x = x;
        this.y = y;
        this.curvature = curvature;
    }

    public Section(int id, boolean hasStation, Captor captor) {
        this.id = id;
        this.hasStation = hasStation;
        this.rails = new TreeMap<Integer, Rail>();
    }

    public Section(int id, Captor captor) {
        this.id = id;
        this.hasStation = false;
        this.rails = new TreeMap<Integer, Rail>();
    }

    public Section(int id) {
        this.id = id;
        this.rails = new TreeMap<Integer, Rail>();
    }

    public Section() {
        this.rails = new TreeMap<Integer, Rail>();
    }

    public Captor getCaptor() {
        return captor;
    }

    public void setCaptor(Captor captor) {
        this.captor = captor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public boolean hasStation() {
        return hasStation;
    }

    public void setStation(String station, int idRail) {
        Rail rail=rails.get(idRail);
        rail.setName(station);
        hasStation = true;

    }

    public TreeMap<Integer, Rail> getRails() {
        return rails;
    }

    public Rail getRail(int id){
        Rail rail;
        rail=rails.get(id);
        if(rail!=null ) {
            return rail;
        }
        return null;
    }

    public void addRail(Rail rail){
        rails.put(rail.getId(),rail);
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", rails=" + rails +
                '}';
    }

    public Rail getRailCaptor(){
        return railCaptor;
    }

    public void setRailCaptor(Rail railCaptor){
        this.railCaptor=railCaptor;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}

