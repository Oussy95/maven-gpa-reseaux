package model;

public class Coordonate {

    private int x;
    private int y;
    /**
     * angle de courbure
     */
    private int curvature;

    public Coordonate(int x,int y, int curvature){
        this.x=x;
        this.y=y;
        this.curvature=curvature;

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getCurvature() {
        return curvature;
    }

    public void setCurvature(int curvature) {
        this.curvature = curvature;
    }
}
