package fr.rphstudio.trainmonitor.launcher;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import model.*;
import org.lwjgl.Sys;
import org.newdawn.slick.Color;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class TrainMonitor extends BasicGameState
{
    //------------------------------------------------
    // PUBLIC CONSTANTS
    //------------------------------------------------
    public static final int ID = 300;


    //------------------------------------------------
    // PRIVATE PROPERTIES
    //------------------------------------------------
    private StateBasedGame gameObject;
    private GameContainer  container;

    private String         version;



    private boolean paused;


    private int x;
    private int s;


    private Rail[] rails= new Rail[12];
    private Train train;

    private static int CENTER_X=1000;
    private static int CENTER_Y=500;

    private Simulation simulation;


    //------------------------------------------------
    // PRIVATE METHODS 
    //------------------------------------------------
    // Get current program version string from file
    private void getVersion()
    {
        // Get display version
        BufferedReader br = null;
        try
        {
            this.version = "";
            br = new BufferedReader(new FileReader("info/version.txt"));
            String line;
            line = br.readLine();
            while(line != null)
            {
                this.version = this.version + line + "\n";
                line = br.readLine();
            }
            if (br != null)
            {
                br.close();
            }
        }
        catch (IOException e)
        {
            throw new Error(e);
        }
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ex)
            {
                throw new Error(ex);
            }
        }
    }

    // Quit game
    private void quitGame()
    {
        this.container.exit();
    }
    // toggle full screen mode
    public void toggleFullScreen()
    {
        try
        {
            this.container.setFullscreen( !this.container.isFullscreen() );
        }
        catch (Exception e)
        {
        }
    }

    public void togglePause(){

        this.paused=!this.paused;


    }






    //------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------
    public TrainMonitor() {
        // Get version
        this.getVersion();
    }


    //------------------------------------------------
    // INIT METHOD
    //------------------------------------------------

    /**
     * C'est à ce niveau la que l'on initialise les objets de la modélisation
     * @param container
     * @param sbGame
     */
    public void init(GameContainer container, StateBasedGame sbGame) {
        // Init fields
        this.container  = container;
        this.gameObject = sbGame;
        // DEFAULT RUNNING
        this.paused=false;


        this.x = 960;
        this.s = 1;
        simulation=new Simulation();

    }


    //------------------------------------------------
    // RENDER METHOD
    //------------------------------------------------
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        // Fit Screen
        //MainLauncher.fitScreen(container, g);

        //Arrive pile poil sur le rail 3-5 =-2
        int index;

        for(index=0;index<simulation.getMaxTrainnb();index++)
            drawTrain(simulation.getTrain(index),g);

        //voir comment dans simulation pouvoir dessiner l'ensemble des rails
        // peut être via la section
      for (index=0;index<simulation.getMaxRailNb()-1;index++) {
          Rail rail = simulation.getRail(index);
          drawRail(rail, g);
      }


        //peut être réduire la hauteur du capteur
        //drawCaptor(new Coordonate(rails[0].getX()+20,rails[0].getY(),0),g);

        // g.drawArc(this.x,540,100,500,0,45);

        // Render version
        g.setColor(Color.white);
        g.drawString(this.version, 15, 1000);
    }





    public void drawTrain(Train train, Graphics g){

        g.setColor( Color.yellow );

        //if(train.getCoordonate().getCurvature()==0) {
        g.drawOval(train.getX()+CENTER_X-3, train.getY()+CENTER_Y-3,
                4, 4);
       /* }
        else{
            //g.drawArc(train.getCoordonate().getX(),train.getCoordonate().getY(),);
        }*/

    }

    /**
     *
     * @param rail pour obtenir le point de départ du rail
     * @param g l'interface a dessiner
     */
    public void drawRail(Rail rail,Graphics g){
        if(rail.getClass().equals(SubRail.class)){
            g.setColor(Color.orange);

            if(rail.getRailsSource().size()>1) {
                for (int index = 0; index < rail.getRailsSource().size(); index++) {
                    g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                            rail.getRailsSource().get(index).getX() + CENTER_X, rail.getRailsSource().get(index).getY() + CENTER_Y);
                }
                g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                        rail.getRailsDestination().get(0).getX() + CENTER_X, rail.getRailsDestination().get(0).getY() + CENTER_Y);
            }
            else {
                for (int index = 0; index < rail.getRailsDestination().size(); index++) {
                    g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                            rail.getRailsDestination().get(index).getX() + CENTER_X, rail.getRailsDestination().get(index).getY() + CENTER_Y);
                }
                g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                        rail.getRailsSource().get(0).getX() + CENTER_X, rail.getRailsSource().get(0).getY() + CENTER_Y);
            }
        }
        else if(rail.getClass().equals(CrossRail.class)){
            g.setColor(Color.magenta);

            for (int index = 0; index < rail.getRailsSource().size(); index++) {
                g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                        rail.getRailsSource().get(index).getX() + CENTER_X, rail.getRailsSource().get(index).getY() + CENTER_Y);
            }

            for (int index = 0; index < rail.getRailsDestination().size(); index++) {
                g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                        rail.getRailsDestination().get(index).getX() + CENTER_X, rail.getRailsDestination().get(index).getY() + CENTER_Y);
            }

        }
        else {
            g.setColor(Color.blue);

            if(rail.getRailsDestination().size()>0) {
                Rail rail2=rail.getRailsDestination().get(0);
                g.drawLine(rail.getX() + CENTER_X, rail.getY() + CENTER_Y,
                        rail2.getX() + CENTER_X, rail2.getY() + CENTER_Y);
            }
        }


        /*int rayon=30;
        g.setColor(Color.red);
        g.drawArc(rail.getX(),rail.getY(),rayon,rayon,0,360);*/

    }

    /**
     *
     * @param x
     * @param y
     * @param g
     */
    public void drawCaptor(int x,int y,Graphics g){
        g.setColor (Color.red);

        g.drawLine(x + 25, y - 5, x, y + 5);

    }

//    POUR LES CROISEMENTS ET LES BIFURCATION
//    public void drawSubRail(){ }
//
//    public void drawCrossRail(){ }



    //------------------------------------------------
    // UPDATE METHOD
    //------------------------------------------------
    public void update(GameContainer container, StateBasedGame game, int delta) {
        // Clear graphics



        if(!paused) {
            container.getGraphics().clear();
            simulation.run();
        }


    }


    //------------------------------------------------
    // KEYBOARD METHODS
    //------------------------------------------------
    @Override
    public void keyPressed(int key, char c)
    {
        // Process game workflow
        switch(key)
        {
            // Quit game by pressing escape
            case Input.KEY_ESCAPE:
                this.container.exit();
                break;

            // display FPS
            case Input.KEY_F:
                this.container.setShowFPS( !this.container.isShowingFPS() );
                break;

            // toggle full screen / Windowed application
            case Input.KEY_F11:
                this.toggleFullScreen();
                break;
            //mettre pause à la simulation
            case Input.KEY_P:
                this.togglePause();

                System.out.println("Paused");
                break;


            // all other keys have no effect
            default :
                break;
        }
    }
    @Override
    public void keyReleased(int key, char c)
    {

    }




    //------------------------------------------------
    // STATE ID METHOD
    //------------------------------------------------
    @Override
    public int getID()
    {
        return ID;
    }


    //------------------------------------------------
    // END OF STATE
    //------------------------------------------------
}