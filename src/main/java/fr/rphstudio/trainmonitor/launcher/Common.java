/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.rphstudio.trainmonitor.launcher;

import org.newdawn.slick.Color;

/**
 *
 * @author GRIGNON FAMILY
 */
public class Common
{
    //---------------------------------------------------------
    // SCREEN
    //---------------------------------------------------------
    // Screen default dimensions (automatically resized, if the screen has a different resolution)
    public static final float   SCREEN_WIDTH   = 1920.0f;
    public static final float   SCREEN_HEIGHT  = 1080.0f;    
    
    //---------------------------------------------------------
    // TRANSITIONS
    //---------------------------------------------------------
        // Colors used during game state transitions
    public static final Color   COLOR_FADE_IN  = Color.orange;
    public static final Color   COLOR_FADE_OUT = Color.yellow;
    // Timings used during game state transitions
    public static final int     TIME_FADE_IN   = 250;
    public static final int     TIME_FADE_OUT  = 250;
    public static final int     TIME_FADE_IN2  = 250;
    public static final int     TIME_FADE_OUT2 = 750;
    // Timing used for intro auto skip
    public final static long    INTRO_PAGE_TIME_MS = 4000;
  
    //---------------------------------------------------------
    // PHYSICS
    //---------------------------------------------------------
    // Physic-Display relationships
    public final static float   NB_PIXELS_PER_METER = 64.0f;
    

    
    
    
}