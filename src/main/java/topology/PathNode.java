package topology;

import model.Rail;

public class PathNode {
    private int g;
    private int h;
    private int f;
    private boolean walkable;
    private Rail currentNode;
    private PathNode parent;

    public PathNode(int g, int h, boolean walkable, Rail currentNode, PathNode parent) {
        this.g = g;
        this.h = h;
        this.walkable = walkable;
        this.currentNode = currentNode;
        this.parent = parent;
    }

    public PathNode(PathNode parent) {
        walkable = true;
        g = h = f = 0;
        parent = parent;
    }

    public PathNode(PathNode parent,Rail currentNode) {
        walkable = true;
        g = h = f = 0;
        this.parent = parent;
        this.currentNode=currentNode;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getF() {
        f = h + g;
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }

    public Rail getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(Rail currentNode) {
        this.currentNode = currentNode;
    }

    public void setParent(PathNode parent) {
        this.parent = parent;
    }

    public PathNode getParent() {
        return parent;
    }

    public void setM_parent(PathNode parent) {
        this.parent = parent;
    }
}
