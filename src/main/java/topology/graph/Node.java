package topology.graph;

/**
 * Classe de sommets
 */
public class Node {
    String nom;
    private int marque;

    public Node(String nn, int mm) {
        nom = nn;
        marque = mm;
    }

    public Node(Node s, int mm) {
        nom = s.nom;
        marque = mm;
    }

    public int valeurMarque() {
        return marque;
    }

    public void modifierMarque(int m) {
        marque = m;
    }

    public boolean equals(Object o) {
        return nom.equals(((Node) o).nom);
    }

    public int compareTo(Object o) {
        Node s = (Node) o;
        return nom.compareTo(s.nom);
    }

    public String toString() {
        return "" + nom;
    }

    public int hashCode() {
        return nom.hashCode();
    }
}