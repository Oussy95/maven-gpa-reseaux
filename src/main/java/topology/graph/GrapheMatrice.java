package topology.graph;

import java.util.*;
import model.*;

/**
 * Graphes implantées dans des "matrices"
 */
public class GrapheMatrice extends Graphe {
    private Vector<Vector<Arc>> M;
    private Numbering numbering;

    public int taille() {
        return M.size();
    }

    public GrapheMatrice() {
        super();
    }

    public GrapheMatrice(int n) {
        numbering = new Numbering(n);
        M = new Vector<Vector<Arc>>(n);
        M.setSize(n);
    }

    public void ajouterSommet(Node s) {
        if (numbering.ajouterElement(s)) {
            int n = taille();
            Vector<Arc> vs = new Vector<Arc>(n);
            vs.setSize(n);
            M.set(numbering.numero(s), vs);
        }
    }

    public boolean existeArc(Node s, Node t) {
        int si = numbering.numero(s);
        int ti = numbering.numero(t);
        return M.get(si).get(ti) != null;
    }

    private boolean existeArc(int i, int j){
        return M.get(i).get(j) != null;
    }

    public void ajouterArc(Node s, Node t, int val){
        ajouterSommet(s);
        ajouterSommet(t);
        int si = numbering.numero(s);
        int ti = numbering.numero(t);
        M.get(si).set(ti, new Arc(s, t, val));
    }

    public int valeurArc(Node s, Node t){
        int si = numbering.numero(s);
        int ti = numbering.numero(t);
        return M.get(si).get(ti).valeur();
    }

    public int valeurArc(int i, int j){
        if (M.get(i).get(j).valeur() > 0)
            return M.get(i).get(j).valeur();
        else return 0;
    }

    public void enleverArc(Node s, Node t){
        int si = numbering.numero(s);
        int ti = numbering.numero(t);
        M.get(si).remove(ti);
    }

    public void modifierValeur(Node s, Node t, int val){
        int si = numbering.numero(s);
        int ti = numbering.numero(t);
        M.get(si).get(ti).modifierValeur(val);
    }

    public LinkedList<Arc> voisins(Node s){
        LinkedList<Arc> l = new LinkedList<Arc>();
        int si = numbering.numero(s);
        for (int j = 0; j < taille(); j++)
            if (existeArc(si, j))
                l.addLast(M.get(si).get(j));
        return l;
    }

    public Collection<Node> sommets(){
        return numbering.elements();
    }

    public GrapheMatrice copie(){
        int n = taille();
        GrapheMatrice G = new GrapheMatrice(n);
        for (int i = 0; i < n; i++) G.ajouterSommet(numbering.elementAt(i));
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (M.get(i).get(j) != null)
                    G.ajouterArc(numbering.elementAt(i), numbering.elementAt(j), valeurArc(i, j));
        return G;
    }

    // charger la matrice depuis un fichier texte
    /*public static int[][] loadMatrice(String file) {
        int[][] M = new int[0][0];
        int nb_gares = 0, id = 0;
        try {
            FileReader fr = new FileReader(file);
            BufferedReader input = new BufferedReader(fr);

            int y = 0;
            nb_gares = parseInt(input.readLine());
            M = new int[nb_gares][nb_gares];
            while (input.ready()) {
                String s = input.readLine(); // On lit ligne par ligne (exclut le \n)
                String nameStation="Station " +String.valueOf(id);
                Train station = new Train(id,1,1,1);
                // Lit la ligne de la matrice et place les cellules
                String[] ss = s.split(" ");
                for (int x = 0; x < nb_gares; x++) {
                    M[x][y] = parseInt(ss[x]);
                }
                y++;
                id++;
            }
            id = 0;
            input.close();
        } catch(Exception exc) {
            System.err.println("Ouverture impossible : fichier inexistant ou mal complété.");
        }
        return M;
    }*/

    public GrapheMatrice deMatrice(int[][] M){
        int n = M.length;
        GrapheMatrice G = new GrapheMatrice(n);
        for (int i = 0; i < n; i++)
            G.ajouterSommet(new Node(i+"", 0));
        for (int i = 0; i < n; i++){
            for (int j = 0; j < M[i].length; j++)
                if (M[i][j] > 0)
                    G.ajouterArc(G.numbering.elementAt(i), G.numbering.elementAt(j), M[i][j]);
        }
        return G;
    }
}