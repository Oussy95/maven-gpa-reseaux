package topology.graph;

import java.util.*;

/**
 * Classe abstraite de graphes
 */
public abstract class Graphe {
    public abstract int taille();

    public abstract Graphe copie();

    public abstract void ajouterSommet(Node s);

    public abstract boolean existeArc(Node s, Node t);

    public abstract void ajouterArc(Node s, Node t, int val);

    public abstract int valeurArc(Node s, Node t);

    public abstract void enleverArc(Node s, Node t);

    public abstract Collection<Node> sommets();
}