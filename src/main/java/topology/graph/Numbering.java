package topology.graph;

import java.util.*;

/**
 * Numérotation des graphes
 */
public class Numbering {
    private int compteur;
    private Hashtable<Node,Integer> HSI;
    private Vector<Node> VS;

    public Numbering(int n){
        compteur = -1;
        HSI = new Hashtable<Node,Integer>();
        VS = new Vector<Node>(n);
        VS.setSize(n);
    }

    public int taille(){
        return VS.size();
    }

    public boolean ajouterElement(Node s){
        if (!HSI.containsKey(s)){
            compteur++;
            HSI.put(s, compteur);
            VS.set(compteur, s);
            return true;
        }
        return false;
    }

    public int numero(Node s){
        return HSI.get(s);
    }

    public Node elementAt(int i){
        return VS.elementAt(i);
    }

    public Vector<Node> elements(){
        return VS;
    }
}