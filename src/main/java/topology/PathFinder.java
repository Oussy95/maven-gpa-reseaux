package topology;

import model.*;
import topology.xml.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PathFinder {
    private static int NODE_DISTANCE_VALUE = 100;

    private Map map;
    private int mapxa, mapya, mapxb, mapyb;
    private ArrayList<Rail> pathList = new ArrayList<>();
    private ArrayList<PathNode> openList = new ArrayList<>();
    private ArrayList<PathNode> closeList = new ArrayList<>();
    private ArrayList<String> stationList = new ArrayList<>();
    private Rail mapStart;
    private Rail mapStop;
    public int bestScoreIndex;
    private PathNode pathNodeNext;


    private int COST_RAIL=10;
    private int COST_STATION=50;
    //private int ;



    public PathFinder(Map map, int mapxa, int mapya, int mapxb, int mapyb) {
        this.map = map;
        this.mapxa = mapxa;
        this.mapya = mapya;
        this.mapxb = mapxb;
        this.mapyb = mapyb;
    }

    public PathFinder(Map map) {
        this.map = map;
    }

    public Map getMap() {
        return map;
    }

    public ArrayList<Rail> getPathList() {
        return pathList;
    }

    public ArrayList<PathNode> getOpenList() {
        return openList;
    }

    public ArrayList<PathNode> getCloseList() {
        return closeList;
    }

    public ArrayList<String> getStationList() {
        return stationList;
    }

    public Rail getMapStart() {
        return mapStart;
    }

    public Rail getMapStop() {
        return mapStop;
    }

    public void setMapStart(Rail mapStart) {
        this.mapStart = mapStart;
    }

    public void setMapStop(Rail mapStop) {
        this.mapStop = mapStop;
    }

    public void initStartStop() {
        mapStart = createRail(mapxa, mapya,0);
        mapStop = createRail(mapxb, mapyb,1);
        addToOpenList(new PathNode(0,cbDist(mapxa, mapya, mapxb, mapyb), true, mapStart, null));
    }

    public Rail createRail(int mapx, int mapy,int opt) {
        //start
        if(opt ==0) {
            mapStart.setX(mapx);
            mapStart.setY(mapy);
            return mapStart;
        }
        //stop
        else{
            mapStop.setX(mapx);
            mapStop.setY(mapy);
            return mapStop;
        }

    }

    public int cbDist(int mapxa,int mapya, int mapxb,int mapyb){
        //return (float) (Math.sqrt((mapxa-mapxb)*(mapxa-mapxb)+(mapya-mapyb)*(mapya-mapyb) + 1*Math.pow(mapza - mapzb, 2)));
        return (Math.abs(mapxa - mapxb)+Math.abs(mapya - mapyb));
    }

    public boolean findPath () {
        initStartStop();
        boolean found = false;
        //PathNode parent=null;
        while(!openList.isEmpty()) {
            pathNodeNext = getsmall(openList, mapStop, mapStart);
            for(int i = 0; i < getNeighbours(pathNodeNext).size(); i++) {
                if(!isOnOpenList(getNeighbours(pathNodeNext).get(i))) {
                    if (!isOnCloseList(getNeighbours(pathNodeNext).get(i)))
                        addToOpenList(new PathNode(10,pathNodeNext.getF(),true,getNeighbours(pathNodeNext).get(i),pathNodeNext));
                }
            }
            removeFromOpenList(pathNodeNext);
            addToCloseList(pathNodeNext);





            if (pathNodeNext.getCurrentNode().getId()==mapStop.getId()/*pathNodeNext.getCurrentNode().getX() == mapStop.getX() && pathNodeNext.getCurrentNode().getY() == mapStop.getY()*/) {
                found = true;
                System.out.println("Chemin trouvé");
                break;
            }


        }
        pathList = createPathList(pathNodeNext);
        return found;
    }


    public boolean findPath (int way) {
        //Src
        if(way==0){
            initStartStop();
            boolean found = false;
            //PathNode parent=null;
            while(!openList.isEmpty()) {
                pathNodeNext = getsmall(openList, mapStop, mapStart);


                for(int i = 0; i < getSource(pathNodeNext).size(); i++) {
                    if(!isOnOpenList(getSource(pathNodeNext).get(i))) {
                        if (!isOnCloseList(getSource(pathNodeNext).get(i)))
                            addToOpenList(new PathNode(COST_RAIL,pathNodeNext.getF(),true,getSource(pathNodeNext).get(i),pathNodeNext));
                    }
                }
                removeFromOpenList(pathNodeNext);
                addToCloseList(pathNodeNext);




                if (pathNodeNext.getCurrentNode().getId()==mapStop.getId()/*pathNodeNext.getCurrentNode().getX() == mapStop.getX() && pathNodeNext.getCurrentNode().getY() == mapStop.getY()*/) {
                    found = true;
                    System.out.println("Chemin trouvé");
                    break;
                }


            }
            pathList = createPathList(pathNodeNext);
            return found;

        }
        //Dest
        else{
            initStartStop();
            boolean found = false;
            //PathNode parent=null;
            while(!openList.isEmpty()) {
                pathNodeNext = getsmall(openList, mapStop, mapStart);


                for(int i = 0; i < getDestination(pathNodeNext).size(); i++) {
                    if(!isOnOpenList(getDestination(pathNodeNext).get(i))) {
                        if (!isOnCloseList(getDestination(pathNodeNext).get(i)))
                            addToOpenList(new PathNode(COST_RAIL,pathNodeNext.getF(),true,getDestination(pathNodeNext).get(i),pathNodeNext));
                    }
                }
                removeFromOpenList(pathNodeNext);
                addToCloseList(pathNodeNext);





                if (pathNodeNext.getCurrentNode().getId()==mapStop.getId()/*pathNodeNext.getCurrentNode().getX() == mapStop.getX() && pathNodeNext.getCurrentNode().getY() == mapStop.getY()*/) {
                    found = true;
                    System.out.println("Chemin trouvé");
                    break;
                }


            }
            pathList = createPathList(pathNodeNext);
            return found;
        }

    }

    private ArrayList<Rail> getDestination(PathNode pathNode) {
        return pathNode.getCurrentNode().getRailsDestination();
    }


    private ArrayList<Rail> getSource(PathNode pathNode) {
        return pathNode.getCurrentNode().getRailsSource();
    }

    public ArrayList<Rail> createPathList (PathNode node) {
        ArrayList<Rail> pathList = new ArrayList<>();
        ArrayList<Rail> finalList = new ArrayList<>();
        stationList = new ArrayList<>();
        String station = "";
        pathList.add(node.getCurrentNode());
        PathNode father = node.getParent();
        while (father != null) {
            pathList.add(father.getCurrentNode());
            father = father.getParent();
        }
        for (int i = pathList.size()-1; i >= 0; i--) {
            finalList.add(pathList.get(i));
            /*if (!pathList.get(i).getName().equals(station)) {
                stationList.add(pathList.get(i).getName());
                station = pathList.get(i).getName();
            }*/
        }
        return finalList;
    }

    public void getNodeList(ArrayList<PathNode> list) {
       /* for (int u=0;u<list.size();u++) {
            System.out.println(list.get(u).getCurrentNode().getX()+":"+list.get(u).getCurrentNode().getY());
            System.out.println("f:"+list.get(u).getF()+" g:"+list.get(u).getG()+" h:"+list.get(u).getH());
        }*/
    }


    private  void removeFromCloseList(PathNode node) {
        ArrayList<PathNode> tmpList = new ArrayList<>();
        int maximum = closeList.size();

        for (PathNode pathNode : closeList) {
            if (pathNode != node)
                tmpList.add(pathNode);
        }
        closeList = tmpList;
    }

    private  void removeFromOpenList(PathNode node) {
        ArrayList<PathNode> tmpList = new ArrayList<>();
        int maximum = openList.size();

        for (PathNode pathNode : openList) {
            if (pathNode != node)
                tmpList.add(pathNode);
        }
        openList = tmpList;
    }

    private void addToCloseList(PathNode node) {
        removeFromOpenList(node);
        closeList.add(node);
    }

    private void addToOpenList(PathNode node) {
        removeFromCloseList(node);
        openList.add(node);

    }

    private PathNode getCurrentNode() {
        ArrayList<PathNode> tmpList = new ArrayList<>();
        int maximum = openList.size();
        int minF = 1000000;
        PathNode curNode = null;

        for (PathNode node : openList) {
            if (node.getF() < minF) {
                minF = node.getF();
                curNode = node;
            }
        }
        return curNode;
    }

    private ArrayList<Rail> getNeighbours(PathNode pathNode) {
        ArrayList<Rail> neighbours = new ArrayList<>();
        Rail rail = pathNode.getCurrentNode();
        neighbours.addAll(rail.getRailsDestination());
        neighbours.addAll(rail.getRailsSource());

        return neighbours;

    }

    public PathNode getsmall(ArrayList<PathNode> list, Rail stop, Rail start) {
        PathNode node;
        int bestScore = Integer.MAX_VALUE;
        bestScoreIndex = Integer.MAX_VALUE;

        for(int i =0;i<list.size();i++) {
            if(bestScore > list.get(i).getF()) {
                bestScore = list.get(i).getF();
                bestScoreIndex = i;
            }
        }
        node=list.get(bestScoreIndex);
        return node;
    }

    private boolean isOnOpenList( Rail rail ) {
        int maximum = openList.size();

        for (PathNode pathNode1 : openList) {
            if (pathNode1.getCurrentNode() == rail)
                return true;
        }
        return false;
    }

    private boolean isOnCloseList( Rail rail ) {
        int maximum = closeList.size();

        for (PathNode pathNode1 : closeList) {
            if (pathNode1.getCurrentNode() == rail)
                return true;
        }
        return false;
    }


}
