package topology.xml;

import model.CrossRail;
import model.Rail;
import model.Section;
import model.SubRail;
import org.newdawn.slick.util.xml.XMLElementList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class XmlToMap {
    private int nbSections = 0;
    private int nbStations = 0;
    private int nbTrains = 0;
    private int nbRails = 0;
    private TreeMap<Integer, Section> sections;
    private TreeMap<Integer, Rail> rails=new TreeMap<Integer, Rail>();

    private TreeMap<Integer, Rail> railsNotInsert=new TreeMap<Integer, Rail>();


    public Map loadMap(String file) {
        int nbRails = 0, idSection = 0, idRail = 0, idNeighbor = 0, idStation = 0, cptRail = 0;
        String type, name = null;
        Rail rail;
        ArrayList<Rail> railSrc, railDest;
        Section section;
        Element elt;
        try {
            File fXmlFile = new File("xml"+File.separator+file);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList mapList = doc.getElementsByTagName("Maquette");
            for (int temp = 0; temp < mapList.getLength(); temp++) {
                Node nNode = mapList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    nbSections = Integer.parseInt(eElement.getAttribute("nbSections"));
                    nbStations = Integer.parseInt(eElement.getAttribute("nbStations"));
                    nbTrains = Integer.parseInt(eElement.getAttribute("nbTrains"));
                }
            }
            Map map = new Map(file, nbSections, nbStations, nbTrains);
            nbRails=doc.getElementsByTagName("rail").getLength();

            NodeList sectList = doc.getElementsByTagName("section");


            for (int i = 0; i < sectList.getLength(); i++) {
                Node n = sectList.item(i);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    elt = (Element) n;
                    idSection = Integer.parseInt(elt.getAttribute("number"));
                    nbRails = Integer.parseInt(elt.getAttribute("nbRails"));
                    section = new Section(idSection);
                    map.getSections().put(idSection, section);
                } else {
                    System.err.println("section not an element");
                    break;
                }

            }

            NodeList railList = doc.getElementsByTagName("rail");

            for (int temp = 0; temp < railList.getLength(); temp++) {
                Node n1 = railList.item(temp);
                Element e1 = (Element) n1;
                Element eSection=(Element) e1.getParentNode();
                idSection=Integer.parseInt(eSection.getAttribute("number"));


                idRail = Integer.parseInt(e1.getAttribute("number"));
                type = e1.getAttribute("type");
                rail=null;
                Rail railExist=isRail(idRail,map);

                if(railExist!=null){
                    rail=railExist;
                    map.getSections().get(idSection).getRails().put(idRail, rail);



                }

                else {
                    name = null;

                    if (type.equals("station")) {
                        idStation = Integer.parseInt(e1.getAttribute("id_station"));
                        name = e1.getAttribute("name");

                        rail=new Rail(idRail,name);
                        map.getSections().get(idSection).getRails().put(idRail, rail);
                        rails.put(rail.getId(),rail);

                    } else if (type.equals("sub_rail")) {
                        rail=new SubRail(idRail);
                        map.getSections().get(idSection).getRails().put(idRail, rail);
                        rails.put(rail.getId(),rail);
                    } else if (type.equals("cross_rail")) {
                        rail=new CrossRail(idRail);
                        map.getSections().get(idSection).getRails().put(idRail, rail);
                        rails.put(rail.getId(),rail);
                    } else {
                        rail=new Rail(idRail);
                        map.getSections().get(idSection).getRails().put(idRail,rail);
                        rails.put(rail.getId(),rail);
                    }

                }


                NodeList neighSrcList = e1.getElementsByTagName("neighborSrc");
                int sizeSrc = neighSrcList.getLength();

                NodeList neighDestList = e1.getElementsByTagName("neighborDest");
                int sizeDest = neighDestList.getLength();

                for (int temp1 = 0; temp1 < sizeSrc; temp1++) {
                    Node n2 = neighSrcList.item(temp1);
                    Element e2 = (Element) n2;
                    idNeighbor = Integer.parseInt(e2.getAttribute("rail_number"));
                    int tmp;
                    tmp=verifyRail(idNeighbor,map);
                    if(tmp!=-1){

                        map.getSections().get(idSection).getRails().get(idRail).addRailSource(map.getSections().get(tmp).getRails().get(idNeighbor));
                        map.getSections().get(tmp).getRails().get(idNeighbor).addRailDestination(map.getSections().get(idSection).getRails().get(idRail));
                    }

                }
                //map.getSections().get(idSection).getRails().get(cptRail).setRailSource(railSrc);

                for (int temp2 = 0; temp2 < sizeDest; temp2++) {
                    Node n2 = neighDestList.item(temp2);
                    Element e2 = (Element) n2;
                    idNeighbor = Integer.parseInt(e2.getAttribute("rail_number"));
                    int tmp;
                    tmp=verifyRail(idNeighbor,map);
                    if(tmp!=-1){
                        map.getSections().get(idSection).getRails().get(idRail).addRailDestination(map.getSections().get(tmp).getRails().get(idNeighbor));
                        map.getSections().get(tmp).getRails().get(idNeighbor).addRailDestination(map.getSections().get(idSection).getRails().get(idRail));
                    }
                }
                //map.getSections().get(idSection).getRails().get(cptRail).setRailDestination(railDest);
                elt = (Element)sectList.item(idSection);
                if(map.getSections().get(idSection).getRails().size()== ((int)Integer.parseInt(elt.getAttribute("nbRails"))/2)+1 ){
                    map.getSections().get(idSection).setRailCaptor(rail);
                }
                cptRail++;
            }

            return map;

        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }



    public Rail isRail(int idRail,Map map){
        return rails.get(idRail);
    }

    private int verifyRail(int id,Map map){
        int size=map.getSections().size();
        for(int index=0;index<size;index++){
            if(map.getSections().get(index).getRail(id)!=null){
                return index;
            }
        }
        return -1;
    }


    private boolean isNotInsert(int id){
        if(railsNotInsert.get(id)!=null){
            return true;
        }
        return false;
    }

    private int[][] createMatriceMap(int nbNode, Map map) {
        int[][] matrice = new int [nbNode][nbNode];
        int cptSection = 0;
        for(int i = 0; i < nbNode; i++) {
            for(int j = 0; j < nbNode; j++) {
                if(i == j) matrice[i][j] = 0;
                else if(i > j) continue;
                else {
                    int val = map.getSections().get(cptSection).getRails().size();
                    matrice[i][j] = val;
                    matrice[j][i] = val;
                }
                cptSection++;
            }
        }
        return matrice;
    }

    public static void main(String[] argv) {
        int cptRail = 0;

        XmlToMap xmlToMap = new XmlToMap();
        //Map map = xmlToMap.loadMap("mapRand.xml");
        Map map2 = xmlToMap.loadMap("map.xml");
        int[][] matrice;
        matrice = xmlToMap.createMatriceMap(xmlToMap.nbStations, map2);

        for(int i = 0; i < xmlToMap.nbStations; i++) {
            for (int j = 0; j < xmlToMap.nbStations; j++) {
                System.out.print(matrice[i][j]+"\t");
            }
            System.out.println();
        }

        /*for (int i = 0; i < map.getSections().size(); i++) {
            System.out.println("Section n°"+ map.getSections().get(i).getId()+" : "+ map.getSections().get(i).getRails());
            for (int j = 0; j < map.getSections().get(i).getRails().size(); j++) {
                System.out.println("\tNeighbors source of "+ map.getSections().get(i).getRails().get(cptRail)+" : "+ map.getSections().get(i).getRails().get(cptRail).getRailsSource());
                System.out.println("\tNeighbors destination of "+ map.getSections().get(i).getRails().get(cptRail)+" : "+ map.getSections().get(i).getRails().get(cptRail).getRailsDestination());
                cptRail++;
            }
        }*/
        System.out.println("loading map ok");
    }
}
