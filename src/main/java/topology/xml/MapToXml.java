package topology.xml;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.HashMap;
import java.util.Random;
import java.util.TreeMap;

import model.*;


public class MapToXml {
    private static final int NB_STATION = 12;
    private static final int NB_SECTION = 26;
    private static final int NB_TRAIN = 3;

    private static final int NB_NODE_MIN = 5;
    private static final int NB_NODE_MAX = 20;
    private static final int NB_RAIL_MIN = 3;
    private static final int NB_RAIL_MAX = 15;


    private static int NB_RAIL = 150 / NB_SECTION;
    private static int cptRail = 0;
    private static int cptStation = 0;


    private void createMap() {
        int id_station = 0;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element model = doc.createElement("Maquette");
            doc.appendChild(model);

            Attr attrStations = doc.createAttribute("nbStations");
            attrStations.setValue(String.valueOf(NB_STATION));
            model.setAttributeNodeNS(attrStations);

            Attr attrSections = doc.createAttribute("nbSections");
            attrSections.setValue(String.valueOf(NB_SECTION));
            model.setAttributeNode(attrSections);

            Attr attrTrains = doc.createAttribute("nbTrains");
            attrTrains.setValue(String.valueOf(NB_TRAIN));
            model.setAttributeNode(attrTrains);

            for(int i = 0; i < NB_SECTION; i++) {
                Element section = doc.createElement("section");
                model.appendChild(section);

                Attr sectNumber = doc.createAttribute("number");
                sectNumber.setValue(String.valueOf(i));
                section.setAttributeNode(sectNumber);

                // Metrique des sections
                if(i == 17 || i == 21) NB_RAIL += 1;
                else if(i == 11 ||  i == 15 ||  i == 2 ||  i == 4 ) NB_RAIL += 2;
                else if(i == 0 || i == 3 || i == 12 || i == 16 || i == 22 || i == 24 || i == 25) NB_RAIL += 3;
                else if(i == 8 || i == 9 || i == 13) NB_RAIL += 4;
                else if(i == 5 || i == 18) NB_RAIL += 5;
                else if(i == 7 || i == 10 || i == 20) NB_RAIL += 6;
                else if(i == 19 || i == 23) NB_RAIL += 7;
                else if(i == 1 || i == 14) NB_RAIL += 9;
                else NB_RAIL += 11;

                Attr attrRails = doc.createAttribute("nbRails");
                attrRails.setValue(String.valueOf(NB_RAIL));
                section.setAttributeNode(attrRails);

                for(int j = 0; j < NB_RAIL; j++) {
                    Element rail = doc.createElement("rail");
                    section.appendChild(rail);

                    // Rails de types STATIONS
                    if((j == NB_RAIL / 2) && (i == 0 || i == 1 || i == 3 || i == 7 || i == 10 || i == 12 || i == 13 || i == 14 || i == 16 || i == 20 || i == 23 || i == 25)) {
                        Attr type = doc.createAttribute("type");
                        type.setValue("station");
                        rail.setAttributeNode(type);

                        Attr statNumber = doc.createAttribute("number");
                        statNumber.setValue(String.valueOf(cptRail + i));
                        rail.setAttributeNode(statNumber);

                        Attr statName = doc.createAttribute("name");
                        statName.setValue(String.valueOf("station"+cptRail + i));
                        rail.setAttributeNode(statName);
                        cptRail++;

                        Attr idstation = doc.createAttribute("id_station");
                        idstation.setValue(String.valueOf(id_station));
                        rail.setAttributeNode(idstation);
                        id_station++;
                    }

                    // Rails de types SUB RAILS
                    else if (((i == 1 || i == 14) && (j == 0 || j == NB_RAIL-1)) || ((i == 4 || i == 17) && (j == NB_RAIL-1)) || ((i == 6 || i == 20) && (j == 0))) {
                        Attr subNumber = doc.createAttribute("number");
                        subNumber.setValue(String.valueOf(cptRail + i));
                        rail.setAttributeNode(subNumber);
                        cptRail++;

                        Attr type = doc.createAttribute("type");
                        type.setValue("sub_rail");
                        rail.setAttributeNode(type);

                    }

                    // Rails de types CROSS RAILS
                    else if (((i == 9 || i == 15) && (j == 0)) || ((i == 8 || i == 13) && (j == NB_RAIL-1)) || ((i == 5 || i == 24) && (j == 0)) || ((i == 10 || i == 18) && (j == 0))) {
                        Attr crossNumber = doc.createAttribute("number");
                        crossNumber.setValue(String.valueOf(cptRail + i));
                        rail.setAttributeNode(crossNumber);
                        cptRail++;

                        Attr type = doc.createAttribute("type");
                        type.setValue("cross_rail");
                        rail.setAttributeNode(type);
                    }

                    // Rails BASIQUES
                    else {
                        Attr railNumber = doc.createAttribute("number");
                        railNumber.setValue(String.valueOf(cptRail + i));
                        rail.setAttributeNode(railNumber);
                        cptRail++;

                        Attr type = doc.createAttribute("type");
                        type.setValue("rail");
                        rail.setAttributeNode(type);
                    }
                    for(int k = 0; k < 1; k++) {
                        if (j > 0) {
                            Element neighborSrc = doc.createElement("neighborSrc");
                            rail.appendChild(neighborSrc);
                            Attr neighSrcNumber = doc.createAttribute("rail_number");
                            neighSrcNumber.setValue(String.valueOf(cptRail + i - 2));
                            neighborSrc.setAttributeNode(neighSrcNumber);
                        }
                        if (j < NB_RAIL-1) {
                            Element neighborDest = doc.createElement("neighborDest");
                            rail.appendChild(neighborDest);
                            Attr neighDestNumber = doc.createAttribute("rail_number");
                            neighDestNumber.setValue(String.valueOf(cptRail + i));
                            neighborDest.setAttributeNode(neighDestNumber);
                        }
                    }
                }
                NB_RAIL = 150 / NB_SECTION;
                cptRail--;
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("xml\\map.xml"));

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);

            System.out.println("File saved!");


        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public int nbRailRandom(int min, int max) {
        int value = (Math.random()<0.5)?0:1;
        if(value == 1) {
            return nbRandom(NB_RAIL_MIN, NB_RAIL_MAX);
        }
        return value;
    }

    public int nbRandom(int min, int max) {
        Random r = new Random();
        int value = min + r.nextInt(max - min);
        return value;
    }

    public int[][] createMatrice(int nbNode) {
        int[][] matrice = new int [nbNode][nbNode];
        for(int i = 0; i < nbNode; i++) {
            for(int j = 0; j < nbNode; j++) {
                if(i == j) matrice[i][j] = 0;
                else if(i > j) continue;
                else {
                    int val = nbRailRandom(0, 1);
                    matrice[i][j] = val;
                    matrice[j][i] = val;
                }
            }
        }
        return matrice;
    }

    public int [][] createMatricMap () {
        int[][] matriceMap = new int[NB_STATION][NB_STATION];

        for(int i = 0; i < 12; i++) {
            for(int j = 0; j < 12; j++) {
                if((i < 5 && j > 5) || (i > 4 && j < 7)) {
                    matriceMap[i][j] = 0;
                }
                else if((i == 0 && j == 1) || i == 6 && j == 7) matriceMap[i][j] = 11;
                else if((i == 1 && j == 2) || i == 7 && j == 8) matriceMap[i][j] = 17;
                else if((i == 1 && j == 4) || i == 7 && j == 10) matriceMap[i][j] = 20;
                else if((i == 2 && j == 3) || i == 8 && j == 9) matriceMap[i][j] = 41;
                else if((i == 2 && j == 5) || i == 8 && j == 11) matriceMap[i][j] = 28;
                else if((i == 3 && j == 1) || i == 9 && j == 7) matriceMap[i][j] = 21;
                else if((i == 4 && j == 3) || i == 10 && j == 9) matriceMap[i][j] = 26;
                else matriceMap[i][j] = 0;
            }
        }
        return matriceMap;
    }

    public void createRandomMap(String file, int[][] mat) {
        HashMap<Integer, Section> map;
        Map mp = new Map();
        map = mp.buildMap(mat);
        cptRail = 0;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element model = doc.createElement("Maquette");
            doc.appendChild(model);

            Attr attrStations = doc.createAttribute("nbStations");
            attrStations.setValue(String.valueOf(mat.length));
            model.setAttributeNodeNS(attrStations);

            Attr attrSections = doc.createAttribute("nbSections");
            attrSections.setValue(String.valueOf(map.size()));
            model.setAttributeNode(attrSections);

            Attr attrTrains = doc.createAttribute("nbTrains");
            attrTrains.setValue(String.valueOf(mat.length/2));
            model.setAttributeNode(attrTrains);

            for(int i = 0; i < map.size(); i++) {
                Element section = doc.createElement("section");
                model.appendChild(section);

                Attr sectNumber = doc.createAttribute("number");
                sectNumber.setValue(String.valueOf(map.get(i).getId()));
                section.setAttributeNode(sectNumber);

                Attr attrRails = doc.createAttribute("nbRails");
                attrRails.setValue(String.valueOf(map.get(i).getRails().size()));
                section.setAttributeNode(attrRails);
                for(int j = 0; j < map.get(i).getRails().size(); j++) {
                    //System.out.println("j :"+map.get(i).getId());
                    Element rail = doc.createElement("rail");
                    section.appendChild(rail);

                    Attr railNumber = doc.createAttribute("number");
                    railNumber.setValue(String.valueOf(cptRail));
                    rail.setAttributeNode(railNumber);

                    int nbNeighboursSrc = map.get(i).getRails().get(cptRail).getRailsSource().size();
                    int nbNeighboursDest = map.get(i).getRails().get(cptRail).getRailsDestination().size();

                    if(map.get(i).hasStation() && j == map.get(i).getRails().size() / 2 ) {
                        Attr type = doc.createAttribute("type");
                        type.setValue("station");
                        rail.setAttributeNode(type);

                        Attr name = doc.createAttribute("name");
                        name.setValue(map.get(i).getRail(cptRail).getName());
                        rail.setAttributeNode(name);

                        Attr id = doc.createAttribute("id_station");
                        id.setValue(String.valueOf(cptStation));
                        rail.setAttributeNode(id);
                        cptStation++;
                    }
                    else if(nbNeighboursSrc > 1 || nbNeighboursDest > 1) {
                        Attr type = doc.createAttribute("type");
                        type.setValue("sub_rail");
                        rail.setAttributeNode(type);
                    }
                    else {
                        Attr type = doc.createAttribute("type");
                        type.setValue("rail");
                        rail.setAttributeNode(type);
                    }

                    for (int k = 0; k < map.get(i).getRails().get(cptRail).getRailsSource().size(); k++) {
                        Element neighbor = doc.createElement("neighborSrc");
                        rail.appendChild(neighbor);

                        Attr neighNumber = doc.createAttribute("rail_number");
                        neighNumber.setValue(String.valueOf(map.get(i).getRails().get(cptRail).getRailsSource().get(k).getId()));
                        neighbor.setAttributeNode(neighNumber);
                    }

                    for (int l = 0; l < map.get(i).getRails().get(cptRail).getRailsDestination().size(); l++) {
                        Element neighbor = doc.createElement("neighborDest");
                        rail.appendChild(neighbor);

                        Attr neighNumber = doc.createAttribute("rail_number");
                        neighNumber.setValue(String.valueOf(map.get(i).getRails().get(cptRail).getRailsDestination().get(l).getId()));
                        neighbor.setAttributeNode(neighNumber);
                    }
                    cptRail++;
                }
            }
            cptStation = 0;
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("xml\\"+file));

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] argv) {
        MapToXml mapToXml = new MapToXml();
        int nbNode = mapToXml.nbRandom(NB_NODE_MIN, NB_NODE_MAX);
        int[][] matrice = mapToXml.createMatrice(NB_NODE_MIN);
        int[][] matriceMap = new int[4][4];


        //mapToXml.createMap();
        //mapToXml.createRandomMap("map.xml", matriceMap);
        mapToXml.createRandomMap("mapRand.xml", matrice);


        for(int i = 0; i < NB_NODE_MIN; i++) {
            for (int j = 0; j < NB_NODE_MIN; j++) {
                System.out.print(matrice[i][j]+"\t");
            }
            System.out.println();
        }
    }
}
