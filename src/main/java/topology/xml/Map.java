package topology.xml;

import model.*;
import topology.graph.*;


import java.util.ArrayList;
import java.util.HashMap;



public class Map {
    private static final int NB_SECTION_MAX = 100;

    private static String file;
    private static int nbSections, nbStations, nbTrains;
    private HashMap<Integer, Section> sections = new HashMap<Integer, Section>();


    public Map(String file) {
        Map.file = file;
    }

    public Map() {
        this.sections = new HashMap<>();
    }

    Map(String file, int nbSections, int nbStations, int nbTrains) {
        Map.file = file;
        Map.nbSections = nbSections;
        Map.nbStations = nbStations;
        Map.nbTrains = nbTrains;
        this.sections = new HashMap<>();
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        Map.file = file;
    }

    public int getNbSections() {
        return nbSections;
    }

    public void setNbSections(int nbSections) {
        Map.nbSections = nbSections;
    }

    public int getNbStations() {
        return nbStations;
    }

    public void setNbStations(int nbStations) {
        Map.nbStations = nbStations;
    }

    public int getNbTrains() {
        return nbTrains;
    }

    public void setNbTrains(int nbTrains) {
        Map.nbTrains = nbTrains;
    }


    public HashMap<Integer, Section> getSections() {
        return sections;
    }

    public void setSections(HashMap<Integer, Section> sections) {
        this.sections = sections;
    }

    /**
     * Ajouter une rail dans une section
     * @param rail rail à ajouter
     * @param idSection l'id de la section
     */
    public void addRailSection(Rail rail,int idSection){
        sections.get(idSection).addRail(rail);
    }

    private void addSectionWithStation(int cptRail, int cptSection, int[] tempRailId, int[] tempSectionId, int i) {
        Section section = new Section(cptSection);
        //ajout d'un rail
        section.addRail(new Rail(cptRail));
        cptRail++;
        //ajout d'un rail station
        section.addRail(new Rail(cptRail, "Station" + cptRail));
        neighbors(section.getRail(cptRail), section.getRail(cptRail - 1), section); //ajout dans la liste des voisins source et destination
        cptRail++;
        //ajout d'une rail
        section.addRail(new Rail(cptRail));
        tempRailId[i] = cptRail;
        tempSectionId[i] = cptSection;
        neighbors(section.getRail(cptRail), section.getRail(cptRail - 1), section); //ajout dans la liste des voisins source et destination

        section.setStation("Station" + cptRail,cptRail); // booléen mis a true pour signifier que la section comporte une station
        getSections().put(cptSection, section); //ajout de la section dans la liste de sections
    }

    private void neighborsInterSection2(int[][] tempRSS, HashMap<Integer, Section> sections, int[][] matrice, int[] tempRailID, int[] tempSectionId) {
        for(int i = 0; i < matrice.length; i++) {
            for(int j = 0; j < tempRSS.length; j ++) {
                if (tempRSS[j][0] == 0 && tempRSS[j][1] == 0 && tempRSS[j][2] == 0) break;
                if (tempRSS[j][0] == i) {
                    int idSrc = sections.get(tempRSS[j][1]).getRails().lastKey();
                    int idDest = sections.get(tempSectionId[i]).getRails().firstKey();
                    neighborsInterSection(sections.get(tempSectionId[i]).getRail(idDest), sections.get(tempRSS[j][1]).getRail(idSrc), sections.get(tempSectionId[i]), sections.get(tempRSS[j][1]));
                }
            }
        }
    }

    private void neighbors(Rail railSource, Rail railDestination, Section section) {
        section.getRail(railSource.getId()).getRailsSource().add(section.getRail(railDestination.getId()));
        section.getRail(railDestination.getId()).getRailsDestination().add(section.getRail(railSource.getId()));
    }

    private void neighborsInterSection(Rail railSource, Rail railDestination, Section sectionSource, Section sectionDestination) {
        sectionSource.getRail(railSource.getId()).getRailsSource().add(sectionDestination.getRail(railDestination.getId()));
        sectionDestination.getRail(railDestination.getId()).getRailsDestination().add(sectionSource.getRail(railSource.getId()));
    }

    public HashMap<Integer, Section> buildMap(int [][] M) {
        int cptSection = 0;
        int cptRail = 0;
        int cptNeighbor = 0;
        int cpt = 0;
        int[] tempRailId = new int[M.length];
        int[] tempSectionId = new int[M.length];
        int[][] tempRSS = new int [NB_SECTION_MAX][3];
        int tempRail = 0, tempSection = 0, tempStation = 0;
        Section section = new Section(cptSection);
        sections = new HashMap<>();
        GrapheMatrice grapheMatrice = new GrapheMatrice();

        GrapheMatrice graphe = grapheMatrice.deMatrice(M); // graphe généré depuis la matrice M

        for (int i = 0; i < graphe.taille(); i++) {
            addSectionWithStation(cptRail, cptSection, tempRailId, tempSectionId, i); // add section with station
            cptRail += 3;
            cptSection++;
            for (int j = 0; j < graphe.taille(); j++) {
                if (i < j && M[i][j] > 0) {
                    cptNeighbor++;
                    for (int l = 0; l < M[i][j]; l++) {
                        if (l == 0) { //une section inter-stations
                            section = new Section(cptSection); // création de la nouvelle section
                            section.addRail(new Rail(cptRail));
                            if(cptSection > 0)
                                neighborsInterSection(section.getRail(cptRail), sections.get(cptSection-cptNeighbor).getRail(tempRailId[i]), section, sections.get(cptSection-cptNeighbor));
                            cptRail++;
                        }
                        else if (l == M[i][j] - 1) { // fin de la section
                            section.addRail(new Rail(cptRail));
                            neighbors(section.getRail(cptRail), section.getRail(cptRail - 1), section);
                            tempRSS[cpt][0] = j;
                            tempRSS[cpt][1] = cptSection;
                            tempRSS[cpt][2] = cptRail;
                            cpt++;
                            getSections().put(cptSection, section); //ajout de la section dans la liste de sections
                            cptSection++;
                            cptRail++;
                        }
                        else {
                            section.addRail(new Rail(cptRail));
                            neighbors(section.getRail(cptRail), section.getRail(cptRail - 1), section); //ajout dans la liste des voisins source et destination
                            cptRail++;
                        }
                    }
                }
            }
            cptNeighbor = 0;
        }
        neighborsInterSection2(tempRSS, sections, M, tempRailId, tempSectionId);
        return sections;
    }

    @Override
    public String toString() {
        return "Map{" +
                "sections=" + sections +
                '}';
    }
}