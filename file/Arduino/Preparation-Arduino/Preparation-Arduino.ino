
const int ID = 0;
byte ETEINT = 0;//0%
byte ACCELERATION = 191;//75%
byte NORMAL = 128;//50%
byte RALENTIR = 64;//25%
int section_powered;
char inputBuffer[10]; 
char  *separateur = {" "};     // Le séparateur
char *message_ACT = "ACT";
char *asking_ID = "ID?";
char *start = "STA";
char *sto = "STO";

boolean isGood;
boolean authentificationIsDone;

struct Section{
    byte pin;
    byte state;
    unsigned long dateLastChange;
    char* name_; 
  };

struct Sensor{
    byte pin;
    bool detect;
    char* name_; 
  };


Section section_1;
Section section_2;
Section section_3;
Section section_4;
Section section_5;
Section section_6;


Sensor sensor_1;
Sensor sensor_2;
Sensor sensor_3;
Sensor sensor_4;
Sensor sensor_5;
Sensor sensor_6;



void setup() {

  authentificationIsDone = false;
Serial.begin(9600); //Opens serial port, sets data rate to 9600 bit/s.
Serial.setTimeout(100);// explication ici
// PWM outputs
 Section section_1 = {3, ETEINT, 0,"1"};
 Section section_2 = {5, ETEINT, 0,"2" };
 Section section_3 = {6, ETEINT, 0,"3" };
 Section section_4 = {9, ETEINT, 0,"4" };
 Section section_5 = {10, ETEINT, 0,"5" };
 Section section_6 = {11, ETEINT, 0,"6" };

 
// Sensor
Sensor sensor_1 = {A0,false,"1"};
Sensor sensor_2 = {A1,false,"2" };
Sensor sensor_3 = {A2,false,"3" };
Sensor sensor_4 = {A3,false,"4" };
Sensor sensor_5 = {A4,false,"5" };
Sensor sensor_6 = {A5,false,"6" };

// Sensor, we will use DigitalRead 
pinMode(A0,INPUT);
pinMode(A1,INPUT);
pinMode(A2,INPUT);
pinMode(A3,INPUT);
pinMode(A4,INPUT);
pinMode(A5,INPUT);
//char inputBuffer[10]; 
while(!authentificationIsDone)
{
  if(Serial.available())
    {
      Serial.readBytes(inputBuffer,10);
      char buf[5],buf2[5],buf3[5];;
      sprintf(buf,"%s",inputBuffer);
      sprintf(buf2,"%s",asking_ID);
      sprintf(buf3,"%s",start);
      if (strncmp(buf,buf2,4)==0)
      {
        Serial.println("0:1-2-3-4-5-6");
      }
      if(strncmp(buf,buf3,4)==0)
      {
        Serial.println("STA-ON");
        authentificationIsDone = true;
        // peut etre rajouter un délais par la suite.
      }
    }
}


isGood = true;
}
void loop() {
  while(isGood)
  {
    if(Serial.available())
    {
      Serial.readBytes(inputBuffer,10);
      {
        char *order;
        order = strtok(inputBuffer,"-");
        char *first = order;
        order = strtok (NULL, "-");
        char *id = order;
        char buf[5],buf2[5],buf3[3];
        sprintf(buf,"%s",first);
        sprintf(buf2,"%s",message_ACT);
        sprintf(buf3,"%s",sto);
        if(strncmp(buf,buf3,4)==0)
        {
          Serial.println("STOP:ON");
          isGood = false;
        }
        else if (strncmp(buf,buf2,4)==0)
        {
          int iD = atoi(id);
          //Serial.println(iD);
          // Confirmation d'un message : actvation tronçon
          switch (iD)
          {
            case 1:
            Serial.println("ACT-1:ON");
            analogWrite(section_1.pin, 191);
            break;
            
            case 2:
            Serial.println("ACT-2:ON");
            analogWrite(section_2.pin, 191);
            break;
            
            case 3:
            Serial.println("ACT-3:ON");
            break;
            
            case 4:
            Serial.println("ACT-4:ON");
            break;
            
            case 5:
            Serial.println("ACT-5:ON");
            break;
            
            case 6:
            Serial.println("ACT-6:ON");
            break;
    
            default:
            break;
          }       
         }
      }
      
  }
    
   }
  
}
