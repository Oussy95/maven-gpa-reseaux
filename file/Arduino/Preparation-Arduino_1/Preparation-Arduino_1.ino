const int ID = 1;
byte ETEINT = 0;//0%
byte ACCELERATION = 191;//75%
byte NORMAL = 128;//50%
byte RALENTIR = 64;//25%
int section_powered;
char inputBuffer[10]; 
char  *separateur = {" "};     // Le séparateur
char *message_ACT = "ACT";
char *asking_ID = "ID?";
char *start = "STA";
char *sto = "STO";

boolean isGood;
boolean authentificationIsDone;

struct Section{
    byte pin;
    byte state;
    unsigned long dateLastChange;
    char* name_; 
  };

struct Sensor{
    byte pin;
    bool detect;
    char* name_; 
  };


Section section_7;
Section section_8;
Section section_9;
Section section_10;
Section section_11;
Section section_12;


Sensor sensor_7;
Sensor sensor_8;
Sensor sensor_9;
Sensor sensor_10;
Sensor sensor_11;
Sensor sensor_12;



void setup() {

  authentificationIsDone = false;
Serial.begin(9600); //Opens serial port, sets data rate to 9600 bit/s.
Serial.setTimeout(100);// explication ici
// PWM outputs
 Section section_7 = {3, ETEINT, 0,"7"};
 Section section_8 = {5, ETEINT, 0,"8" };
 Section section_9 = {6, ETEINT, 0,"9" };
 Section section_10 = {9, ETEINT, 0,"10" };
 Section section_11 = {10, ETEINT, 0,"11" };
 Section section_12 = {11, ETEINT, 0,"12" };

 
// Sensor
Sensor sensor_7 = {A0,false,"7"};
Sensor sensor_8 = {A1,false,"8" };
Sensor sensor_9 = {A2,false,"9" };
Sensor sensor_10 = {A3,false,"10" };
Sensor sensor_11 = {A4,false,"11" };
Sensor sensor_12 = {A5,false,"12" };

// Sensor, we will use DigitalRead 
pinMode(A0,INPUT);
pinMode(A1,INPUT);
pinMode(A2,INPUT);
pinMode(A3,INPUT);
pinMode(A4,INPUT);
pinMode(A5,INPUT);
//char inputBuffer[10]; 
while(!authentificationIsDone)
{
  if(Serial.available())
    {
      Serial.readBytes(inputBuffer,10);
      char buf[5],buf2[5],buf3[5];;
      sprintf(buf,"%s",inputBuffer);
      sprintf(buf2,"%s",asking_ID);
      sprintf(buf3,"%s",start);
      if (strncmp(buf,buf2,4)==0)
      {
        Serial.println("1:7-8-9-10-11-12");
      }
      if(strncmp(buf,buf3,4)==0)
      {
        Serial.println("STA-ON");
        authentificationIsDone = true;
        // peut etre rajouter un délais par la suite.
      }
      
    }
    delay(10);
}


isGood = true;
}
void loop() {
  while(isGood)
  {
    if(Serial.available())
    {
      Serial.readBytes(inputBuffer,10);
      {
        char *order;
        order = strtok(inputBuffer,"-");
        char *first = order;
        order = strtok (NULL, "-");
        char *id = order;
        char buf[5],buf2[5],buf3[3];
        sprintf(buf,"%s",first);
        sprintf(buf2,"%s",message_ACT);
        sprintf(buf3,"%s",sto);
        if(strncmp(buf,buf3,4)==0)
        {
          Serial.println("STOP:ON");
          isGood = false;
        }
        else if (strncmp(buf,buf2,4)==0)
        {
          int iD = atoi(id);
          //Serial.println(iD);
          // Confirmation d'un message : actvation tronçon
          switch (iD)
          {
            case 7:
            Serial.println("ACT-7:ON");
            analogWrite(section_7.pin, 191);
            break;
            
            case 8:
            Serial.println("ACT-8:ON");
            analogWrite(section_8.pin, 191);
            break;
            
            case 9:
            Serial.println("ACT-9:ON");
            break;
            
            case 10:
            Serial.println("ACT-10:ON");
            break;
            
            case 11:
            Serial.println("ACT-11:ON");
            break;
            
            case 12:
            Serial.println("ACT-12:ON");
            break;
    
            default:
            break;
          }       
         }
      }
      
  }
    
   }
  
}
